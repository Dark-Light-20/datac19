/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
 * Proyecto DataC19
 * Autor: Scrum Team 1
 * Fecha: 17-02-21
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
 */

package com.DataC19;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@ManagedBean(value="symptomReportBean")
@SessionScoped
public class SymptomReportBean implements Serializable{
	// -------------------------------------------------------------
	// Constantes
	// -------------------------------------------------------------

	private static final long serialVersionUID=1L;

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------
	
	//--------------- Secci�n Reporte
	/*
	 * Lista de las personas
	 */
	private ArrayList<Persona> people = new ArrayList<Persona>();
	
	/*
	 * Lista de nombres de sintomas
	 */
	private ArrayList<String> symptomsNames = new ArrayList<String>();

	/*
	 * Identificaci�n temporal para busqueda
	 */
	private String tmpID;
	
	/*
	 * Indice temporal de la posicion de la persona encontrada dentro del listado de personas
	 */
	private int tmpIndex;
	
	/*
	 * Nombres y Apellidos temporales para resultado de busqueda
	 */
	private String tmpNames;
	
	/*
	 * Rol temporal para resultado de busqueda
	 */
	private String tmpRole;
	
	/*
	 * Bandera para determinar si fue encontrada o no una persona
	 */
	private boolean found=false;
	
	/*
	 * Arreglo para controlar los sintomas que reporta la persona
	 */
	private boolean[] tmpSymptoms = new boolean[12];
	
	/*
	 * Cadena para los otros sintomas que reporta la persona
	 */
	private String tmpOther;
	
	// ------------ Secci�n HOME
	/*
	 * Variables que controlan los datos de la persona y su estado con respecto al virus para que
	 * los muestre en la vista 
	 */
	private String homeNames = "";
	private String homeLastNames = "";
	private String homeStatus = "";
	private String homeRole = "";
	private String homeIsReport = "";
	private String homeIsProbe = "";
	private String homeIsContact = "";
	private String homeUnvStatus = "";
	private String homeUnvStatusClass = "";
	private String homeIsSymptoms = "";
	private String homeSymptoms = "";
	
	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------

	@PostConstruct
	public void init() {
		loadPeople();
		loadSymptomsNames();
	}
	
	/**
	 * Permite cargar un conjunto de personas de prueba.
	 */
	private void loadPeople()
	{
		people.add(new Persona("1000000000", "Luis", "Arteaga", "ESTUDIANTE"));
		people.add(new Persona("1000000001", "Angela", "Bola�os", "DOCENTE"));
		people.add(new Persona("1000000002", "Carlos", "Perez", "ADMINISTRATIVO"));
	}
	
	/**
	 * Permite cargar el conjunto de nombres de sintomas del virus.
	 */
	private void loadSymptomsNames() {
		symptomsNames.add("Fiebre");
		symptomsNames.add("Tos seca");
		symptomsNames.add("Cansancio o Dificultad para Respirar");
		symptomsNames.add("Dolores y Molestias");
		symptomsNames.add("Dolor de Cabeza");
		symptomsNames.add("Cangestion nasal");
		symptomsNames.add("Conjuntivitis");
		symptomsNames.add("Dolor de Garganta");
		symptomsNames.add("Diarrea");
		symptomsNames.add("Perdida de Gusto");
		symptomsNames.add("Perdida de Olfato");
		symptomsNames.add("Erupciones Cut�neas");
	}
	
	/**
	 * Permite buscar una persona dentro de la lista de personas.
	 */
	public void search()
	{
		String msg;
		FacesMessage facesMsg;
		tmpSymptoms = new boolean[12];
		tmpOther = "";
		// Iterar el listado de personas en busca de coincidencia entre el ID ingresado en el formulario
		// y las identifiaciones del listado.
		for(int i = 0; i<people.size();i++) {
			if(people.get(i).getIdentification().equals(tmpID)) {
				// Persona encontrada
				found=true;
				tmpIndex = i;
				tmpNames = people.get(i).getName() + " " + people.get(i).getLastname();
				tmpRole = people.get(i).getRole();
				// Mensaje de busqueda existosa.
				msg = "Persona encontrada satisfactoriamente. "+tmpNames+". "+tmpRole;
				facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
				FacesContext.getCurrentInstance().addMessage("form:txtIde", facesMsg);
				// Se renderiza la vista de home
				searchHome();
				return;
			}
		};
		// Mensaje de error, busqueda fallida.
		found=false;
		msg = "La persona no se encuentra registrada en el sistema";
		facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);
		FacesContext.getCurrentInstance().addMessage("form:txtIde", facesMsg);
	}
	
	/**
	 * Permite registrar sintomas a la persona buscada.
	 */
	public void recordSymptoms() {
		// Se verifica que una persona haya sido encontrada
		// tmpID posee un valor
		if(found) {
			// Se registran sintomas a la persona
			ArrayList<String> symptoms = new ArrayList<String>();
			for(int i=0;i<tmpSymptoms.length;i++) if(tmpSymptoms[i]) symptoms.add(symptomsNames.get(i));
			// Se registran otros sintomas a la persona
			String[] others = tmpOther.split(",");
			for(String symptom: others) symptoms.add(symptom);
			// Se agregan los sintomas a la persona
			people.get(tmpIndex).setSymptoms(true);
			people.get(tmpIndex).setSymptomsReport(symptoms);
			System.out.println(people.get(tmpIndex).getName()+" "+people.get(tmpIndex).getLastname()+" =>"+people.get(tmpIndex).getSymptomsReport());
		} else {
			// No se ha buscado la persona
			String msg = "Debe ingresar su identficaci�n para registrar sintomas";
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);
			FacesContext.getCurrentInstance().addMessage("reportForm:registryButton", facesMsg);
		}
		// Se reinician las variables temporales de busqueda
		tmpID = "";
		found = false;
		tmpNames = "";
		tmpRole = "";
		found=false;
		tmpSymptoms = new boolean[13];
		tmpOther = "";
		try {
			// Se redirige a la p�gina de busqueda
			FacesContext.getCurrentInstance().getExternalContext().redirect("search.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Permite visualizar los datos y estado de una persona
	 */
	public void searchHome() {
		// Se verifica que una persona haya sido encontrada
		// tmpID posee un valor
		if(found) {
			// Se asignan los datos de la persona en las variables textuales de la vista Home
			homeNames = people.get(tmpIndex).getName();
			homeLastNames = people.get(tmpIndex).getLastname();
			if(people.get(tmpIndex).isInfected()) homeStatus = "Si";
			else homeStatus = "No";
			homeRole = people.get(tmpIndex).getRole();
			// Se valida qu� informaci�n con respecto al virus presenta la persona para mostrar su estado de ingreso
			// a la universidad
			if(people.get(tmpIndex).isSymptoms()) {
				homeIsReport = "Si";
				homeUnvStatus = "No seguro";
				homeUnvStatusClass = "text-danger";
			}
			else {
				homeIsReport = "No";
				homeUnvStatus = "Seguro";
				homeUnvStatusClass = "text-success";
			}
			if(people.get(tmpIndex).isProbe()) homeIsProbe = "Si";
			else homeIsProbe = "No";
			if(people.get(tmpIndex).isContact()) {
				homeIsContact = "Si";
				if(!people.get(tmpIndex).isSymptoms()) {
					homeUnvStatus = "Poco seguro";
					homeUnvStatusClass = "text-warning";
				}
			}
			else homeIsContact = "No";
			// Si la persona presenta sintomas �stos son mostrados en la vista
			if(!people.get(tmpIndex).getSymptomsReport().isEmpty()) {
				homeIsSymptoms = "true";
				homeSymptoms = people.get(tmpIndex).getSymptomsReport().toString();
			}
			else {
				homeIsSymptoms = "false";
				homeSymptoms = "";
			}
		} else {
			// Si no se encuentra a la persona la vista aparece vac�a
			homeNames = "";
			homeLastNames = "";
			homeStatus = "";
			homeRole = "";
			homeIsReport = "";
			homeIsProbe = "";
			homeIsContact = "";
			homeUnvStatus = "";
			homeUnvStatusClass = "";
			homeIsSymptoms = "false";
			homeSymptoms = "";
		}
	}


	/*
	 * Getters and Setters
	 */
	
	public ArrayList<Persona> getPeople() {
		return people;
	}

	public void setPeople(ArrayList<Persona> people) {
		this.people = people;
	}
	
	public String getTmpID() {
		return tmpID;
	}

	public void setTmpID(String tmpID) {
		this.tmpID = tmpID;
	}

	public int getTmpIndex() {
		return tmpIndex;
	}

	public void setTmpIndex(int tmpIndex) {
		this.tmpIndex = tmpIndex;
	}

	public String getTmpNames() {
		return tmpNames;
	}

	public void setTmpNames(String tmpNames) {
		this.tmpNames = tmpNames;
	}

	public String getTmpRole() {
		return tmpRole;
	}

	public void setTmpRole(String tmpRole) {
		this.tmpRole = tmpRole;
	}

	public boolean isFound() {
		return found;
	}

	public void setFound(boolean found) {
		this.found = found;
	}

	public boolean[] getTmpSymptoms() {
		return tmpSymptoms;
	}

	public void setTmpSymptoms(boolean[] tmpSymptoms) {
		this.tmpSymptoms = tmpSymptoms;
	}

	public String getTmpOther() {
		return tmpOther;
	}

	public void setTmpOther(String tmpOther) {
		this.tmpOther = tmpOther;
	}

	public String getHomeNames() {
		return homeNames;
	}

	public void setHomeNames(String homeNames) {
		this.homeNames = homeNames;
	}

	public String getHomeLastNames() {
		return homeLastNames;
	}

	public void setHomeLastNames(String homeLastNames) {
		this.homeLastNames = homeLastNames;
	}

	public String getHomeStatus() {
		return homeStatus;
	}

	public void setHomeStatus(String homeStatus) {
		this.homeStatus = homeStatus;
	}

	public String getHomeRole() {
		return homeRole;
	}

	public void setHomeRole(String homeRole) {
		this.homeRole = homeRole;
	}

	public String getHomeIsReport() {
		return homeIsReport;
	}

	public void setHomeIsReport(String homeIsReport) {
		this.homeIsReport = homeIsReport;
	}

	public String getHomeIsProbe() {
		return homeIsProbe;
	}

	public void setHomeIsProbe(String homeIsProbe) {
		this.homeIsProbe = homeIsProbe;
	}

	public String getHomeIsContact() {
		return homeIsContact;
	}

	public void setHomeIsContact(String homeIsContact) {
		this.homeIsContact = homeIsContact;
	}

	public String getHomeUnvStatus() {
		return homeUnvStatus;
	}

	public void setHomeUnvStatus(String homeUnvStatus) {
		this.homeUnvStatus = homeUnvStatus;
	}

	public String getHomeUnvStatusClass() {
		return homeUnvStatusClass;
	}

	public void setHomeUnvStatusClass(String homeUnvStatusClass) {
		this.homeUnvStatusClass = homeUnvStatusClass;
	}

	public String getHomeIsSymptoms() {
		return homeIsSymptoms;
	}

	public void setHomeIsSymptoms(String homeIsSymptoms) {
		this.homeIsSymptoms = homeIsSymptoms;
	}

	public String getHomeSymptoms() {
		return homeSymptoms;
	}

	public void setHomeSymptoms(String homeSymptoms) {
		this.homeSymptoms = homeSymptoms;
	}

}
