/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
 * Proyecto DataC19
 * Autor: Scrum Team 1
 * Fecha: 17-02-21
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
 */

package com.DataC19;

import java.io.Serializable;
import java.util.ArrayList;

public class Persona implements Serializable{
	// -------------------------------------------------------------
    // Constantes
    // -------------------------------------------------------------
	
	/*
	 * Manejo del modelo
	 */
	private static final long serialVersionUID = 1L;
	
	// -------------------------------------------------------------
    // Atributos
    // -------------------------------------------------------------

	/*
	 * Identificación de la persona
	 */
	private String identification;
	
	/*
	 * Nombre de la perosna
	 */
	private String name;
	
	/*
	 * Apelldios de la persona
	 */
	private String lastname;	
	
	/*
	 * Rol de la persona
	 */
	private String role;
	
	/*
	 * Estado de contagio
	 */
	private boolean infected;	
	
	/*
	 * Reporta sintomas
	 */
	private boolean symptoms;
	
	/*
	 * Sintomas reportados
	 */
	private ArrayList<String> symptomsReport;
	
	/*
	 * Reporta prueba de virus
	 */
	private boolean probe;
	
	/*
	 * Reporta contacto con positivo
	 */
	private boolean contact;
	
	// -------------------------------------------------------------
    // Métodos
    // -------------------------------------------------------------
	
	/**
	 * Constructor de la clase Persona
	 * @param identification Identificación de la persona. identification != ""
	 * @param name Nombres de la persona. name != ""
	 * @param lastname Apellidos de la persona. lastname != ""
	 * @param role Rol de la persona. role=="ESTUDIANTE" || role=="DOCENTE" || role=="ADMINISTRATIVO"
	 */
	public Persona(String identification, String name, String lastname, String role) {
		super();
		this.identification = identification;
		this.name = name;
		this.lastname = lastname;
		this.role = role;
		this.infected = false;
		this.symptoms = false;
		this.symptomsReport = new ArrayList<String>();
		this.probe= false;
		this.contact= false;	
	}

	/**
	 * Devuelve la identicación de la persona
	 * @return id de la persona
	 */
	public String getIdentification() {
		return identification;
	}

	/**
	 * Modifica la identificación de la persona
	 * @param identification Identificación de la persona. identification != ""
	 */
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	/**
	 * Devuelve los nombres de la persona
	 * @return nombres de la persona
	 */
	public String getName() {
		return name;
	}

	/**
	 * Modifica los nombres de la persona
	 * @param name Nombres de la persona. name != ""
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Devuelve los apellidos de la persona
	 * @return apellidos de la persona
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Modifica los apellidos de la persona
	 * @param lastname Apellidos de la persona. lastname != ""
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * Devuelve el estado de contagio de la persona
	 * @return estado de contagio de la persona
	 */
	public boolean isInfected() {
		return infected;
	}

	/**
	 * Modifica el estado de contagio de la persona
	 * @param infected Estado de contagio de la persona. infected==true || infected==false 
	 */
	public void setInfected(boolean infected) {
		this.infected = infected;
	}
	
	/**
	 * Devuelve el listado de sintomas reportados de la persona
	 * @return listado de sintomas reportados de la persona
	 */
	public ArrayList<String> getSymptomsReport() {
		return symptomsReport;
	}

	/**
	 * Modifica el listado de sintomas reportados de la persona
	 * @param symptomsReport Listado de sintomas reportados de la persona. symptomsReport != []
	 */
	public void setSymptomsReport(ArrayList<String> symptomsReport) {
		this.symptomsReport = symptomsReport;
	}

	/**
	 * Devuelve el rol de la persona
	 * @return rol de la persona
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Modifica el rol de la persona
	 * @param role Rol de la persona. role=="ESTUDIANTE" || role=="DOCENTE" || role=="ADMINISTRATIVO"  
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Devuelve el estado de sintomas de la persona
	 * @return estado de sintomas de la persona
	 */
	public boolean isSymptoms() {
		return symptoms;
	}

	/**
	 * Modifica el estado de sintomas de la persona
	 * @param sympthoms Estado de sintomas de la persona. symptoms==true || symptoms==false 
	 */
	public void setSymptoms(boolean symptoms) {
		this.symptoms = symptoms;
	}

	/**
	 * Devuelve el estado de reporte de prueba de la persona
	 * @return estado de reporte de prueba de la persona
	 */
	public boolean isProbe() {
		return probe;
	}

	/**
	 * Modifica el estado de reporte de prueba de la persona
	 * @param probe Estado de reporte de prueba de la persona. probe==true || probe==false 
	 */
	public void setProbe(boolean probe) {
		this.probe = probe;
	}

	/**
	 * Devuelve el estado de contacto con positivo de la persona
	 * @return estado de contacto con positivo de la persona
	 */
	public boolean isContact() {
		return contact;
	}

	/**
	 * Modifica el estado de contacto con positivo de la persona
	 * @param contact Estado de contacto con positivo de la persona. contact==true || contact==false 
	 */
	public void setContact(boolean contact) {
		this.contact = contact;
	}	
}
